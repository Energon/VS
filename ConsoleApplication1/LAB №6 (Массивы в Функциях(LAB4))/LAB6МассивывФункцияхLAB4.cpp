// ConsoleApplication4.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>
#include <cmath>

void summ_matr(int arr_a[3][3], int arr_b[3][3], int arr_c[3][3])
{
	std::cout << "Addition of matrices" << std::endl;//  Сложение матриц
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			arr_c[i][j] = 0;
			{
				arr_c[i][j] = arr_a[i][j] + arr_b[i][j];
				std::cout << arr_c[i][j] << '\t';//вывод результата сложения матриц
			}
		}
		std::cout << std::endl;
	}


	std::cout << std::endl;
}
	

void summ_vec(int vec_a[1][3], int vec_b[1][3], int vec_c[1][3])
{
	std::cout << "Addition of vectors" << std::endl;
	for (int i = 0; i < 3; i++)
	{
		vec_c[0][i] = vec_a[0][i] + vec_b[0][i];
		std::cout << vec_c[0][i] << "\t";
	}
	std::cout << std::endl;
	std::cout << std::endl;
}


void umn_matr(int arr_a[3][3], int arr_b[3][3], int arr_c[3][3])
{
	std::cout << "Multiplication of matrices" << std::endl;// Умножение матриц


	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			arr_c[i][j] = 0;
			{
				for (int k = 0; k < 3; k++)
					arr_c[i][j] += arr_a[i][k] * arr_b[k][j];
			}
			std::cout << arr_c[i][j] << "\t ";//вывод результата умножения матриц
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;
}

void umn_matr_and_vec(int arr_b[3][3], int vec_a[1][3], int arr_c[3][3])
{
	std::cout << "Multiplication of matrice and vector" << std::endl;// Умножение матрицы на вектор

	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			arr_c[i][j] = 0;
			{
				arr_c[i][0] += arr_b[i][j] * vec_a[0][j];
			}
		}
		std::cout << arr_c[i][0] << std::endl;//Вывод результата умножения матрицы на вектор
	}

	std::cout << std::endl;

}

void skal_umn_vec(int vec_rez[1][3], int vec_a[1][3], int vec_b[1][3])
{
	std::cout << "Skalyar multiplication of vectors" << std::endl;//Скалярное умножение векторов


	for (int i = 0; i < 1; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			vec_rez[i][j] = 0;
			{
				vec_rez[i][0] += vec_a[i][j] * vec_b[0][j];
			}
		}
		std::cout << vec_rez[i][0] << std::endl;//Вывод результата скалярного умножения векторов
	}

	std::cout << std::endl;
}

void vec_umn_vec(int vec_rez[1][3],int vec_AxB[3][3])
{
	std::cout << "Vector multiplication of vectors" << std::endl;//Векторное умножение векторов


	vec_rez[0][0] = vec_AxB[1][1] * vec_AxB[2][2] - vec_AxB[1][2] * vec_AxB[2][1];//1-ая координата вектора
	vec_rez[0][1] = vec_AxB[1][2] * vec_AxB[2][0] - vec_AxB[1][0] * vec_AxB[2][2];//2-ая координата вектора
	vec_rez[0][2] = vec_AxB[1][0] * vec_AxB[2][1] - vec_AxB[1][1] * vec_AxB[2][0];//3-ая координата вектора

	for (int i = 0; i < 1; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			std::cout << vec_rez[i][j] << "      ";//Вывод результата векторного умножение векторов
		}

	}

	std::cout << std::endl;
}

int main()
{
	int arr_a[3][3] = { { 1, 2, 3 },
						 { 4, 5, 6 },
						 { 7, 8, 9 } };
	int arr_b[3][3] = { { 10, 11, 12 },
						 { 13, 14, 15 },
						 { 16, 17, 18 } };
	int arr_c[3][3] = { { 0, 0, 0 },
						 { 0, 0, 0 },
						 { 0, 0, 0 } };
	int vec_a[1][3] = { { 1, 2, 3 } };
	int vec_b[1][3] = { { 7, 9, 4 } };
	int vec_rez[1][3] = { { 0, 0, 0 } };
	int x = 1;
	int	y = 1;
	int	z = 1;//x, y, z, - единичные векторы
	int vec_AxB[3][3] = { { x, y, z },//vec_AxB - векторное произведение vec_a и vec_b в декартовой системе координат
						  { vec_a[0][0], vec_a[0][1], vec_a[0][2] },
						  { vec_b[0][0], vec_b[0][1], vec_b[0][2] } };
	
	

	summ_matr(arr_a, arr_b, arr_c);
	umn_matr(arr_a, arr_b, arr_c);
	summ_vec(vec_a, vec_b, vec_rez);
	umn_matr_and_vec(arr_b, vec_a, arr_c);
	skal_umn_vec(vec_rez, vec_a, vec_b);
	vec_umn_vec(vec_rez, vec_AxB);
	getchar();
	return 0;
}


